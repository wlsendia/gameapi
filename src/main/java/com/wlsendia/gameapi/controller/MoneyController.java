package com.wlsendia.gameapi.controller;

import com.wlsendia.gameapi.model.MoneyResponse;
import com.wlsendia.gameapi.model.SingleResult;
import com.wlsendia.gameapi.service.MoneyService;
import com.wlsendia.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "돈 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/money")
public class MoneyController {
    private final MoneyService moneyService;

    @ApiOperation(value = "돈 증가(광석캐기)")
    @PutMapping("/plus")
    public SingleResult<MoneyResponse> plusMoney() {
        return ResponseService.getSingleResult(moneyService.putMoneyPlus());
    }
}
