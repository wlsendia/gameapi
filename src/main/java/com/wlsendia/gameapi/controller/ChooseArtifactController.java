package com.wlsendia.gameapi.controller;

import com.wlsendia.gameapi.model.ChooseArtifactResponse;
import com.wlsendia.gameapi.model.SingleResult;
import com.wlsendia.gameapi.service.ArtifactChooseService;
import com.wlsendia.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "유물 뽑기")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/choose-artifact")
public class ChooseArtifactController {
    private final ArtifactChooseService artifactChooseService;

    @ApiOperation(value = "은상자 뽑기")
    @PostMapping("/silver")
    public SingleResult<ChooseArtifactResponse> chooseSilverBox() {
        return ResponseService.getSingleResult(artifactChooseService.getResult(false));
    }

    @ApiOperation(value = "금상자 뽑기")
    @PostMapping("/gold")
    public SingleResult<ChooseArtifactResponse> chooseGoldBox() {
        return ResponseService.getSingleResult(artifactChooseService.getResult(true));
    }
}
