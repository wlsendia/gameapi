package com.wlsendia.gameapi.controller;

import com.wlsendia.gameapi.model.CommonResult;
import com.wlsendia.gameapi.model.FirstConnectDataResponse;
import com.wlsendia.gameapi.model.SingleResult;
import com.wlsendia.gameapi.service.GameDataService;
import com.wlsendia.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "게임 데이터 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/first-connect")
public class GameDataController {
    private final GameDataService gameDataService;

    @ApiOperation(value = "첫 접속시 현황 데이터")
    @GetMapping("/init")
    public SingleResult<FirstConnectDataResponse> getFirstData() {
        return ResponseService.getSingleResult(gameDataService.getFirstData());
    }

    @ApiOperation(value = "게임 정보 리셋")
    @DeleteMapping("/reset")
    public CommonResult resetGameData() {
        gameDataService.gameReset();

        return ResponseService.getSuccessResult();
    }
}
