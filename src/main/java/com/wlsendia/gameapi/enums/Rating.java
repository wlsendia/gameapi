package com.wlsendia.gameapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rating {
    LV1("노멀"),
    LV2("희귀"),
    LV3("레어"),
    LV4("유니크"),
    LV5("전설"),
    LV6("신화"),
    ;

    private final String name;
}
