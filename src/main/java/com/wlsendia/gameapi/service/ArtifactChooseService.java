package com.wlsendia.gameapi.service;

import com.wlsendia.gameapi.entity.GameArtifact;
import com.wlsendia.gameapi.entity.Money;
import com.wlsendia.gameapi.entity.UseArtifact;
import com.wlsendia.gameapi.exception.CMissingDataException;
import com.wlsendia.gameapi.model.*;
import com.wlsendia.gameapi.repository.GameArtifactRepository;
import com.wlsendia.gameapi.repository.MoneyRepository;
import com.wlsendia.gameapi.repository.UseArtifactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ArtifactChooseService {
    private final MoneyRepository moneyRepository;
    private final GameArtifactRepository gameArtifactRepository;
    private final UseArtifactRepository useArtifactRepository;

    long choosePay = 0L;

    private void init(boolean isGoldBox) {
        this.choosePay = isGoldBox ? 35000L : 5000L; // 뽑기비용... 금박스 여는거면 35000원, 은박스 여는거면 5000원
    }

    public ChooseArtifactResponse getResult(boolean isGoldBox) {
        this.init(isGoldBox); // 초기화 시킨다.

        boolean isEnoughMoney = this.isStartChooseByMoneyCheck(isGoldBox); // 돈 충분한지 확인한다.

        if (!isEnoughMoney) throw new CMissingDataException(); // 돈이 충분하지 않으면 뽑기 진행 불가능.. 예외처리 exception은 알아서 추가하기

        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new);
        money.minusMoney(this.choosePay); // 뽑기 비용만큼 돈 소모시키기
        moneyRepository.save(money); // 돈 마이너스 시킨거 저장해서 확정하기

        long artifactResultId = this.getChooseArtifact(isGoldBox); // 랜덤으로 뽑은 유물 id 받아오기

        boolean isNewArtifact = false; // 새로 뽑은 유물인지 검사하기 위해 변수 추가하는데 기본으로 아니라고(false) 함.
        Optional<UseArtifact> useArtifact = useArtifactRepository.findByGameArtifact_Id(artifactResultId); // 뽑힌 유물 id와 일치하는 보유 유물 데이터를 가져온다.
        if (useArtifact.isEmpty()) throw new CMissingDataException(); // 만약 보유유물 데이터가 없으면 게임진행이 안되므로 오류메세지 출력..
        UseArtifact useArtifactData = useArtifact.get(); // 명확성을 위해 UseArtifact 모양으로 상자 하나 만들어서 거기다가 원본 옮겨놓기

        if (useArtifactData.getArtifactCount() == 0) isNewArtifact = true; // 만약 원본데이터에서 보유유물 갯수가 0개라면 이건 없던걸 뽑은거니까 isNewArtifact를 true로 바꿔주기

        useArtifactData.putCountPlus(); // 보유유물 갯수 +1 해주기
        useArtifactRepository.save(useArtifactData); // 갯수 + 1 해준거.. (수정된거) 반영 확정하기

        // 위에서 처리는 다 되었고 이제 정보 돌려줄 준비 하기
        ChooseArtifactResponse result = new ChooseArtifactResponse();
        result.setMoneyResponse(new MoneyResponse.MoneyResponseBuilder(money).build());
        result.setCurrentItem(new ChooseArtifactCurrentItem.ChooseArtifactCurrentItemBuilder(useArtifactData.getGameArtifact(), isNewArtifact).build());
        result.setUseArtifactItems(this.getMyArtifacts());

        return result;
    }

    /**
     * 내가 보유한 유물 컬렉션을 가져온다.
     *
     * @return 보유한 유물 컬렉션
     */
    public List<UseArtifactItem> getMyArtifacts() {
        List<UseArtifact> originList = useArtifactRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L); // 내가 가지고있는 유물 리스트 전체를 불러온다.

        List<UseArtifactItem> result = new LinkedList<>(); // 결과값을 담을 빈 리스트를 생성한다.

        originList.forEach(item -> result.add(new UseArtifactItem.UseArtifactItemBuilder(item).build()));

        return result;
    }


    /**
     * 뽑기를 진행함에 있어 돈이 충분한지 확인해주는 메서드
     *
     * @param isGoldBox 금박스 뽑기 여부
     * @return true : 충분하다 / false : 불충분하다
     */
    private boolean isStartChooseByMoneyCheck(boolean isGoldBox) {
        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈 데이터 가져오기.. 얘는 무조건 1번이니까 1번으로 가져오면 된다...

        boolean result = false; // 기본으로 일단 막아두기...

        if (money.getPayCount() >= choosePay) result = true; // 내가 가진돈이 뽑기비용보다 많거나 같으면 체크 결과를 true로 바꾼다.

        return result;
    }

    /**
     * 랜덤으로 유물 하나 뽑아오기
     *
     * @param isGoldBox 금박스 뽑기 여부
     * @return 랜덤으로 뽑힌 유물시퀀스
     */
    private long getChooseArtifact(boolean isGoldBox) {
        List<GameArtifact> artifacts = gameArtifactRepository.findAll();

        List<ChooseArtifactItem> percentBar = new LinkedList<>(); // 확률bar 결과 넣는 리스트

        double oldPercent = 0D; // 확률 누적값 담을 변수 생성
        for (GameArtifact artifact : artifacts) { // 유물 리스트에서 유물을 하나씩 던져주면서 반복시작한다.
            ChooseArtifactItem addItem = new ChooseArtifactItem(); // 확률bar를 채우기 위한 새 그릇을 만든다.
            addItem.setId(artifact.getId()); // 확률bar용 그릇에 유물id값을 넣는다.
            addItem.setPercentMin(oldPercent); // 확률bar용 그릇에 확률 시작값 세팅
            if (isGoldBox) { // 만약에 금상자를 여는것이라면
                addItem.setPercentMax(oldPercent + artifact.getGoldPercent()); // 확률bar용 그릇에 확률 종료값 세팅 (금 확률값)
            } else { // 그게 아니라면.. (= 은상자 여는거)
                addItem.setPercentMax(oldPercent + artifact.getSilverPercent()); // 확률bar용 그릇에 확률 종료값 세팅 (은 확률값)
            }

            percentBar.add(addItem); // 확률bar 결과 넣는 리스트에 위에서 만든 그릇.. 세팅 다 된 그릇 추가

            if (isGoldBox) {
                oldPercent += artifact.getGoldPercent(); // 확률 누적값에 확률 누적 (금 확률값)
            } else {
                oldPercent += artifact.getSilverPercent(); // 확률 누적값에 확률 누적 (은 확률값)
            }

        }

        double percentResult = Math.random() * 100; // 랜덤으로 0~100 사이 수 하나 뽑아오기

        long resultId = 0; // 뽑기 결과 유물id 담을 변수 만들기
        for (ChooseArtifactItem item : percentBar) { // 확률bar 안에 아이템 검사하면서 랜덤으로 뽑은 수치가 어느 구간에 해당되는지 검사하기위해 반복 시작
            if (percentResult >= item.getPercentMin() && percentResult <= item.getPercentMax()) { // 만약 랜덤으로 뽑은 수가 현재 구간의 최소값보다 크거나 같고 랜덤으로 뽑은 수가 현재 구간의 최대값보다 작거나 같으면
                resultId = item.getId(); // 뽑기 결과 유물id에 현재 구간의 id를 뽑아다가 넣고
                break; // 반복 종료하기
            }
        }

        return resultId;
    }
}
