package com.wlsendia.gameapi.service;

import com.wlsendia.gameapi.entity.Money;
import com.wlsendia.gameapi.exception.CMissingDataException;
import com.wlsendia.gameapi.model.MoneyResponse;
import com.wlsendia.gameapi.repository.MoneyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;

    public MoneyResponse putMoneyPlus() {
        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈은 무조건 1번 데이터에 있으니까 1번으로 가져오기..
        money.plusMoney(); // 엔티티 안쪽에 put기능을 하는 메서드를 이용해서 500원 증가시키기

        Money result = moneyRepository.save(money); // 저장하고 저장한 결과를 받아오기

        return new MoneyResponse.MoneyResponseBuilder(result).build(); // MoneyResponse를 주기로 약속했으니까 Money를 MoneyResponse로 바꿔서 돌려주기
    }
}
