package com.wlsendia.gameapi.service;

import com.wlsendia.gameapi.entity.Money;
import com.wlsendia.gameapi.entity.UseArtifact;
import com.wlsendia.gameapi.exception.CMissingDataException;
import com.wlsendia.gameapi.model.FirstConnectDataResponse;
import com.wlsendia.gameapi.model.MoneyResponse;
import com.wlsendia.gameapi.model.UseArtifactItem;
import com.wlsendia.gameapi.repository.MoneyRepository;
import com.wlsendia.gameapi.repository.UseArtifactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameDataService {
    private final MoneyRepository moneyRepository;
    private final UseArtifactRepository useArtifactRepository;

    /**
     * 게임 접속하면 맨 처음 받아올 데이터
     *
     * @return
     */
    public FirstConnectDataResponse getFirstData() {
        FirstConnectDataResponse result = new FirstConnectDataResponse();                        // 결과값을 담을 빈 그릇 생성.. 여기는 빌더 쓰지 않아서 new FirstConnectDataResponse()해서 빈 그릇 만들어 줄 수 있다.

        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new);             // 돈 원본 데이터 가져오기
        result.setMoneyResponse(new MoneyResponse.MoneyResponseBuilder(money).build());             // 돈정보 가공해서 넣기
        result.setUseArtifactItems(this.getMyArtifacts());                                      // 보유 유물 컬렉션 넣기

        return result;
    }

    /**
     * 게임 정보 리셋
     */
    public void gameReset() {
        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈 원본 데이터 가져오기
        money.resetMoney(); // 돈 0원으로 교체
        moneyRepository.save(money); // 저장해서 확정

        List<UseArtifact> originList = useArtifactRepository.findAll(); // 내가 가지고있는 유물 리스트 전체를 불러온다.
        for (UseArtifact item : originList) { // 유물 리스트에서 유물 하나씩 던져주면서 반복시작
            item.resetCount(); // 유물 보유 수량 0으로 리셋하고
            useArtifactRepository.save(item); // 저장해서 확정
        }
    }

    /**
     * 내가 보유한 유물 컬렉션을 가져온다.
     *
     * @return 보유한 유물 컬렉션
     */
    public List<UseArtifactItem> getMyArtifacts() {
        List<UseArtifact> originList = useArtifactRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L); // 내가 가지고있는 유물 리스트 전체를 불러온다.

        List<UseArtifactItem> result = new LinkedList<>(); // 결과값을 담을 빈 리스트를 생성한다.

        originList.forEach(item -> result.add(new UseArtifactItem.UseArtifactItemBuilder(item).build()));

        return result;
    }
}
