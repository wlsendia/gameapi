package com.wlsendia.gameapi.configure;

import com.wlsendia.gameapi.service.InitDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WebRunner implements ApplicationRunner {
    private final InitDataService initDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        initDataService.setFirstMoney(); // 돈 데이터 세팅해주기..
        initDataService.setFirstArtifact(); // 유물 데이터 세팅해주기..
        initDataService.setFirstUseArtifact(); // 보유 유물 데이터 세팅해주기..
    }
}
