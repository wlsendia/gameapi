package com.wlsendia.gameapi.model;

import com.wlsendia.gameapi.entity.Money;
import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyResponse {
    @ApiModelProperty(notes = "돈 수량")
    private Long payCount;

    private MoneyResponse(MoneyResponseBuilder builder) {
        this.payCount = builder.payCount;
    }

    public static class MoneyResponseBuilder implements CommonModelBuilder<MoneyResponse> {
        private final Long payCount;

        public MoneyResponseBuilder(Money money) {
            this.payCount = money.getPayCount();
        }

        @Override
        public MoneyResponse build() {
            return new MoneyResponse(this);
        }
    }
}
