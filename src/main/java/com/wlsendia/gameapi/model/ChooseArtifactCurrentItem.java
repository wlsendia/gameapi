package com.wlsendia.gameapi.model;

import com.wlsendia.gameapi.entity.GameArtifact;
import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ChooseArtifactCurrentItem {
    @ApiModelProperty(notes = "이미지명")
    private String imageName;

    @ApiModelProperty(notes = "유물명")
    private String itemName;

    @ApiModelProperty(notes = "등급값")
    private String rating;

    @ApiModelProperty(notes = "등급명(한글명)")
    private String ratingName;

    @ApiModelProperty(notes = "새로운 유물 획득 여부")
    private Boolean isNew;

    private ChooseArtifactCurrentItem(ChooseArtifactCurrentItemBuilder builder) {
        this.imageName = builder.imageName;
        this.itemName = builder.itemName;
        this.rating = builder.rating;
        this.ratingName = builder.ratingName;
        this.isNew = builder.isNew;
    }

    public static class ChooseArtifactCurrentItemBuilder implements CommonModelBuilder<ChooseArtifactCurrentItem> {
        private final String imageName;
        private final String itemName;
        private final String rating;
        private final String ratingName;
        private final Boolean isNew;

        public ChooseArtifactCurrentItemBuilder(GameArtifact gameArtifact, Boolean isNew) {
            this.imageName = gameArtifact.getImageName();
            this.itemName = gameArtifact.getItemName();
            this.rating = gameArtifact.getRating().toString();
            this.ratingName = gameArtifact.getRating().getName();
            this.isNew = isNew;
        }

        @Override
        public ChooseArtifactCurrentItem build() {
            return new ChooseArtifactCurrentItem(this);
        }
    }
}
