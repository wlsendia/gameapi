package com.wlsendia.gameapi.model;

import com.wlsendia.gameapi.entity.UseArtifact;
import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UseArtifactItem {
    @ApiModelProperty(notes = "이미지명")
    private String imageName;

    @ApiModelProperty(notes = "유물명")
    private String itemName;

    @ApiModelProperty(notes = "등급값")
    private String rating;

    @ApiModelProperty(notes = "등급명(한글명)")
    private String ratingName;

    @ApiModelProperty(notes = "수량")
    private Integer artifactCount;

    private UseArtifactItem(UseArtifactItemBuilder builder) {
        this.imageName = builder.imageName;
        this.itemName = builder.itemName;
        this.rating = builder.rating;
        this.ratingName = builder.ratingName;
        this.artifactCount = builder.artifactCount;
    }

    public static class UseArtifactItemBuilder implements CommonModelBuilder<UseArtifactItem> {
        private final String imageName;
        private final String itemName;
        private final String rating;
        private final String ratingName;
        private final Integer artifactCount;

        public UseArtifactItemBuilder(UseArtifact useArtifact) {
            this.imageName = useArtifact.getGameArtifact().getImageName();
            this.itemName = useArtifact.getGameArtifact().getItemName();
            this.rating = useArtifact.getGameArtifact().getRating().toString();
            this.ratingName = useArtifact.getGameArtifact().getRating().getName();
            this.artifactCount = useArtifact.getArtifactCount();
        }

        @Override
        public UseArtifactItem build() {
            return new UseArtifactItem(this);
        }
    }
}
