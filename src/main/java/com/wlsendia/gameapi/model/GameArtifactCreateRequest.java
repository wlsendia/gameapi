package com.wlsendia.gameapi.model;

import com.wlsendia.gameapi.enums.Rating;
import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameArtifactCreateRequest {
    @ApiModelProperty(notes = "등급")
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Rating rating;

    @ApiModelProperty(notes = "유물명")
    @NotNull
    @Length(min = 2, max = 20)
    private String itemName;

    @ApiModelProperty(notes = "이미지명")
    @NotNull
    @Length(min = 2, max = 50)
    private String imageName;

    @ApiModelProperty(notes = "은상자 확률")
    @NotNull
    private Double silverPercent;

    @ApiModelProperty(notes = "금상자 확률")
    @NotNull
    private Double goldPercent;

    private GameArtifactCreateRequest(GameArtifactCreateRequestBuilder builder) {
        this.rating = builder.rating;
        this.itemName = builder.itemName;
        this.imageName = builder.imageName;
        this.silverPercent = builder.silverPercent;
        this.goldPercent = builder.goldPercent;
    }

    public static class GameArtifactCreateRequestBuilder implements CommonModelBuilder<GameArtifactCreateRequest> {
        private final Rating rating;
        private final String itemName;
        private final String imageName;
        private final Double silverPercent;
        private final Double goldPercent;

        public GameArtifactCreateRequestBuilder(Rating rating, String itemName, String imageName, Double silverPercent, Double goldPercent) {
            this.rating = rating;
            this.itemName = itemName;
            this.imageName = imageName;
            this.silverPercent = silverPercent;
            this.goldPercent = goldPercent;
        }

        @Override
        public GameArtifactCreateRequest build() {
            return new GameArtifactCreateRequest(this);
        }
    }
}
