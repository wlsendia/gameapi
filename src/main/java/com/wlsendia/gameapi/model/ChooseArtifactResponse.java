package com.wlsendia.gameapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChooseArtifactResponse {
    @ApiModelProperty(notes = "돈 정보")
    private MoneyResponse moneyResponse;

    @ApiModelProperty(notes = "뽑은 유물 정보")
    private ChooseArtifactCurrentItem currentItem;

    @ApiModelProperty(notes = "나의 유물 리스트")
    private List<UseArtifactItem> useArtifactItems;
}
