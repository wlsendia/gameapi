package com.wlsendia.gameapi.repository;

import com.wlsendia.gameapi.entity.UseArtifact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UseArtifactRepository extends JpaRepository<UseArtifact, Long> {
    Optional<UseArtifact> findByGameArtifact_Id(long gameArtifactId);
    List<UseArtifact> findAllByIdGreaterThanEqualOrderByIdAsc(long id);
}
