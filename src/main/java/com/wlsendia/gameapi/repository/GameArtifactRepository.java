package com.wlsendia.gameapi.repository;

import com.wlsendia.gameapi.entity.GameArtifact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameArtifactRepository extends JpaRepository<GameArtifact, Long> {
}
