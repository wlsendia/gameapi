package com.wlsendia.gameapi.repository;

import com.wlsendia.gameapi.entity.Money;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<Money, Long> {
}
