package com.wlsendia.gameapi.entity;

import com.wlsendia.gameapi.enums.Rating;
import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import com.wlsendia.gameapi.model.GameArtifactCreateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameArtifact {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "등급")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Rating rating;

    @ApiModelProperty(notes = "유물명")
    @Column(nullable = false, length = 20)
    private String itemName;

    @ApiModelProperty(notes = "이미지명")
    @Column(nullable = false, length = 50)
    private String imageName;

    @ApiModelProperty(notes = "은상자 확률")
    @Column(nullable = false)
    private Double silverPercent;

    @ApiModelProperty(notes = "금상자 확률")
    @Column(nullable = false)
    private Double goldPercent;

    private GameArtifact(GameArtifactBuilder builder) {
        this.rating = builder.rating;
        this.itemName = builder.itemName;
        this.imageName = builder.imageName;
        this.silverPercent = builder.silverPercent;
        this.goldPercent = builder.goldPercent;
    }

    public static class GameArtifactBuilder implements CommonModelBuilder<GameArtifact> {
        private final Rating rating;
        private final String itemName;
        private final String imageName;
        private final Double silverPercent;
        private final Double goldPercent;

        public GameArtifactBuilder(GameArtifactCreateRequest createRequest) {
            this.rating = createRequest.getRating();
            this.itemName = createRequest.getItemName();
            this.imageName = createRequest.getImageName();
            this.silverPercent = createRequest.getSilverPercent();
            this.goldPercent = createRequest.getGoldPercent();
        }

        @Override
        public GameArtifact build() {
            return new GameArtifact(this);
        }
    }
}
