package com.wlsendia.gameapi.entity;

import com.wlsendia.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Money {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "돈 수량")
    @Column(nullable = false)
    private Long payCount;

    public void resetMoney() {
        this.payCount = 0L;
    }

    public void plusMoney() {
        this.payCount += 500;
    }

    public void minusMoney(long money) {
        this.payCount -= money;
    }

    private Money(MoneyBuilder builder) {
        this.payCount = builder.payCount;
    }

    public static class MoneyBuilder implements CommonModelBuilder<Money> {
        private final Long payCount;

        public MoneyBuilder() {
            this.payCount = 0L;
        }

        @Override
        public Money build() {
            return new Money(this);
        }
    }
}