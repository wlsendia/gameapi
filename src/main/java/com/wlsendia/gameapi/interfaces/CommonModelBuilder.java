package com.wlsendia.gameapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
