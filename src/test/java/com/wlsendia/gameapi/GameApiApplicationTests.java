package com.wlsendia.gameapi;

import com.wlsendia.gameapi.entity.GameArtifact;
import com.wlsendia.gameapi.model.ChooseArtifactItem;
import com.wlsendia.gameapi.repository.GameArtifactRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;

@SpringBootTest
class GameApiApplicationTests {
	@Autowired
	private GameArtifactRepository gameArtifactRepository;

	@Test
	void contextLoads() {
	}

	@Test
	void testPercent() {
		List<GameArtifact> artifacts = gameArtifactRepository.findAll();

		List<ChooseArtifactItem> percentBar = new LinkedList<>(); // 확률bar 결과 넣는 리스트

		double oldPercent = 0D; // 확률 누적값 담을 변수 생성
		for (GameArtifact artifact : artifacts) { // 유물 리스트에서 유물을 하나씩 던져주면서 반복시작한다.
			ChooseArtifactItem addItem = new ChooseArtifactItem(); // 확률bar를 채우기 위한 새 그릇을 만든다.
			addItem.setId(artifact.getId()); // 확률bar용 그릇에 유물id값을 넣는다.
			addItem.setPercentMin(oldPercent); // 확률bar용 그릇에 확률 시작값 세팅
			addItem.setPercentMax(oldPercent + artifact.getSilverPercent()); // 확률bar용 그릇에 확률 종료값 세팅

			percentBar.add(addItem); // 확률bar 결과 넣는 리스트에 위에서 만든 그릇.. 세팅 다 된 그릇 추가

			oldPercent += artifact.getSilverPercent(); // 확률 누적값에 확률 누적
		}

		double percentResult = Math.random() * 100; // 랜덤으로 0~100 사이 수 하나 뽑아오기

		long resultId = 0; // 뽑기 결과 유물id 담을 변수 만들기
		for (ChooseArtifactItem item : percentBar) { // 확률bar 안에 아이템 검사하면서 랜덤으로 뽑은 수치가 어느 구간에 해당되는지 검사하기위해 반복 시작
			if (percentResult >= item.getPercentMin() && percentResult <= item.getPercentMax()) { // 만약 랜덤으로 뽑은 수가 현재 구간의 최소값보다 크거나 같고 랜덤으로 뽑은 수가 현재 구간의 최대값보다 작거나 같으면
				resultId = item.getId(); // 뽑기 결과 유물id에 현재 구간의 id를 뽑아다가 넣고
				break; // 반복 종료하기
			}
		}

		System.out.println(resultId);
	}

}
